const tableNames = require('./tableNames');

module.exports = [
    tableNames.item_image,
    tableNames.related_item,
    tableNames.item_info,
    tableNames.retailer,
    tableNames.inventory_location,
    tableNames.item,
    tableNames.size,
    tableNames.shape,
    tableNames.company,
    tableNames.address,
    tableNames.state,
    tableNames.country,
    tableNames.user,
    tableNames.item_type
]